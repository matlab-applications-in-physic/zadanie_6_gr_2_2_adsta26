/*
Matlab applications in physics
Author: Adam Stasiak
Engineering Physics
*/

//installation of the JSON Toolbox for Scilab
//if JSON Toolbox for Scilab has not been installed before, you must reset Scilab after installation
atomsInstall('json');

//function for calculating wind chill factor
function twc = WindChill(t, v)
   if(v > 4.8 && t < 10.0) //for other values the result is incorrect
        twc = round(13.2 + 0.6215*t - 11.37*v^0.16 + 0.3965*t*v^0.16); //source of the formula: https://en.wikipedia.org/wiki/Wind_chill
    else
        twc = t;
    end

endfunction

//function for adding the line to a csv file
function WriteToCsv(content, pathToFile)
   outputFile = mopen(pathToFile, 'a+'); //opening the file
   n = size(content)(2) //number of columns
   for i = 1:(n - 1)
      mfprintf(outputFile,  content(i) + ','); //writing data separated by a comma
   end
   mfprintf(outputFile, content(n) + '\n') //writing the newline character after last data
   mclose(outputFile); //closing the file
endfunction

//function for converting time format
function time = ConvertTime( time12h )
   time = strsplit(time12h, ':');
   if time(1) == '12'
      time(1) = '00';
   end
   time(1) = string(strtod(time(1)) + 12);
   time = time(1) + ':' + time(2);
endfunction

//function for displaying on the screen a horizontal separator with a given number of characters
function PrintLine(sign, len)
   for i = 1:len
      mprintf(sign);
   end
   mprintf('\n');
endfunction

//function for displaying on the screen a table title
function PrintTitle(title, len, separator)
   lenToWord = string(int( ( len - 2 - length(title) )/2 + length(title) )); //distance in characters from the left edge of the table to the end of the title
   lenToSeparator = string(int(len - 2 - strtod(lenToWord) + 1 )); //distance in characters from the end of the title to the right edge of the table
   mprintf('%s%' + lenToWord + 's%' + lenToSeparator + 's\n', separator, title, separator); //displaying the title in the right place on the screen
endfunction

//function for displaying a table row on the screen
function PrintData(data, cellsWidth, separator)
      mprintf(separator);
      for i = 1:size(data)(2) //loop running as many times as there were cells
         lenToWord = string(int( ( cellsWidth(i) - length(data(i)) )/2 + length(data(i)) )); //distance in characters from the left edge of the cell to the end of the name
         lenToSeparator = string(int(cellsWidth(i) - strtod(lenToWord) + 1 )); //distance in characters from the end of the name to the right edge of the cell
         mprintf('%' + lenToWord + 's%' + lenToSeparator + 's', data(i), separator); //displaying one table cell
      end
      mprintf('\n'); //transition to the new line
endfunction

path = get_absolute_file_path('weather.sce'); //path to the folder where this script is located
filename = 'weatherData.csv'; //name of the data file

//headers for the csv file
headerlineToCsv = ['Źródlo', 'Stacja', 'Data pomiaru', 'Godzina pomiaru', ...
                  'Temperatura [°C]', 'Temperatura odczuwalna [°C]', ...
                  'Ciśnienie [hPa]', 'Prędkość wiatru [km/h]', 'Wilgotność [%%]'];

//headers for the current values table
firstHeaderlineForActual = ['Źródlo', 'Stacja', 'Data pomiaru', 'Godzina', 'Temperatura', ...
                           'Temperatura', 'Ciśnienie', 'Prędkość', 'Wilgotność'];
secondHeaderlineForActual = ['', '', '', 'pomiaru', '[°C]', 'odczuwalna', '[hPa]', 'wiatru', '[%]'];
thirdHeaderlineForActual = ['', '', '', '', '', '[°C]', '', '[km/h]', ''];

//headers for the extreme values table
headerlineForExtremal = ['Wielkość', 'Wartość', 'Data pomiaru', 'Godzina pomiaru', 'Stacja', 'Źródło'];

stationNamesImgw = ['bielskobiala', 'raciborz', 'katowice']; //station names for danepubliczne.imgw.pl
stationNamesWttr = ['bielsko-biala', 'raciborz', 'katowice']; //station names for wttr.in
stationNames = ['Bielsko-Biała', 'Racibórz', 'Katowice']; //station names for displaying
cellsWidthForActual = [23, 15, 14, 9, 13, 13, 11, 10, 12] //width of successive cells of the current values ​​table
cellsWidthForExtremal = [29, 9, 14, 17, 15, 23]; //width of successive cells of the extreme values ​​table
dataSources = ['danepubliczne.imgw.pl', 'wttr.in']; //data sources

//checking if a data file exists
//if the file does not exist, the script calls the function that creates the file and write the header line in it
if find(listfiles(path)==filename) == []
   WriteToCsv(headerlineToCsv, path + filename);
end


while %T

   //download data from the station
   for i = 1:3
      dataImgw(i) = JSONParse(mgetl(getURL('https://danepubliczne.imgw.pl/api/data/synop/station/' + stationNamesImgw(i))));
      dataWttr(i) = JSONParse(mgetl(getURL('http://wttr.in/' + stationNamesWttr(i) + '?format=j1')));
   end

   //time format conversion for data downloaded from wttr.in
   for i = 1:3
      dataWttr(i).current_condition.observation_time = strsubst(dataWttr(i).current_condition.observation_time, ',' , ':');
      if (strstr(dataWttr(i).current_condition.observation_time, 'AM')) == ''
            dataWttr(i).current_condition.observation_time = strsubst(dataWttr(i).current_condition.observation_time, ' PM' , '');
            dataWttr(i).current_condition.observation_time = ConvertTime(dataWttr(i).current_condition.observation_time);
      else
         dataWttr(i).current_condition.observation_time = strsubst(dataWttr(i).current_condition.observation_time, ' AM' , '');
      end
   end

   //preparation of empty lists for basic data from the station
   basicInfoImgw = list();
   basicInfoWttr = list();
   windChillImgw = list();
   //recording of basic data
   for i = 1:3
      //calculation of wind chill for data from danepubliczne.imgw.pl
      windChillImgw(i) = [dataImgw(i).data_pomiaru, dataImgw(i).godzina_pomiaru + ':00', string(WindChill(strtod(dataImgw(i).temperatura), ...
                          strtod(dataImgw(i).predkosc_wiatru)))];

      //basic data for danepubliczne.imgw.pl (in order: data source, station name, measurement date, measurement time,
      //temperature, wind chill, pressure, wind speed, humidity)
      basicInfoImgw(i) = [dataSources(1), stationNames(i), dataImgw(i).data_pomiaru, dataImgw(i).godzina_pomiaru + ':00', ...
                          dataImgw(i).temperatura, windChillImgw(i)(3), dataImgw(i).cisnienie, dataImgw(i).predkosc_wiatru, ...
                          dataImgw(i).wilgotnosc_wzgledna];

      //basic data for wttr.in (in order: data source, station name, measurement date, measurement time,
      //temperature, wind chill, pressure, wind speed, humidity)
      basicInfoWttr(i) = [dataSources(2), stationNames(i), dataWttr(i).weather.date(1),  dataWttr(i).current_condition.observation_time, ...
                          dataWttr(i).current_condition.temp_C, dataWttr(i).current_condition.FeelsLikeC, dataWttr(i).current_condition.pressure, ...
                          dataWttr(i).current_condition.windspeedKmph, dataWttr(i).current_condition.humidity];
   end

   data =  csvRead(path + filename, ',', '.', 'string'); //reading data to check that the measurements are not repeated

   //checking that the measurements are not repeated
   //checking is done by checking that the measurement time (date and time) is not repeated
   for i = 1:3
      if( (find(data(:,3)==basicInfoImgw(i)(3)) == []) || (find(data(:,4)==basicInfoImgw(i)(4)) == []) )
         WriteToCsv(basicInfoImgw(i), path + filename);
      end
      if( (find(data(:,3)==basicInfoWttr(i)(3)) == []) || (find(data(:,4)==basicInfoWttr(i)(4)) == []) )
         WriteToCsv(basicInfoWttr(i), path + filename);
      end
   end

   data =  csvRead(path + filename, ',', '.', 'string'); //reading updated data

   //preparation of empty lists for extremal data
   maxValues = [];
   maxIndexes = list();
   minValues = [];
   minIndexes = list();
   //searching for extreme data
   for i = 5:9
      maxValues(i-4, 1) = headerlineToCsv(i); //adding parameter name
      maxValues(i-4, 2) = string(max(strtod(data(:, i)))); //searching for maximum data
      maxIndexes(i-4) = find(string(strtod(data(:,i)))==maxValues(i-4, 2)); //maximum data index search
      minValues(i-4, 1) = headerlineToCsv(i); //adding parameter name
      minValues(i-4, 2) = string(min(strtod(data(:, i)))); //searching for minimum data
      minIndexes(i-4) = find(string(strtod(data(:,i)))==minValues(i-4, 2)); //minimum data index search
   end
   //name correction from 'Wilgotność [%%]'' to 'Wilgotność [%]'
   maxValues(5,1) = 'Wilgotność [%]';
   minValues(5,1) = 'Wilgotność [%]';

   //preparation of empty lists for extreme data matched to the table
   minData = [];
   maxData = [];
   //extreme data counter
   counterMin = 1;
   counterMax = 1;
   //formatting extreme data to fit the extreme data table
   for i = 1:size(minIndexes)(1)
      for j = 1:size(minIndexes(i))(2)
         if j>1
            //adding repetitive data to list
            minData(counterMin,:) = ['', '', data(minIndexes(i)(j),3), data(minIndexes(i)(j),4), data(minIndexes(i)(j),2), data(minIndexes(i)(j),1)];
            counterMin = counterMin + 1;
         else
            //write data to the list (in order: physical quantity, value, measurement date, measurement time, station name, data source)
            minData(counterMin,:) = [minValues(i,1), minValues(i,2), data(minIndexes(i)(j),3), data(minIndexes(i)(j),4), data(minIndexes(i)(j),2), data(minIndexes(i)(j),1)];
            counterMin = counterMin + 1;
         end
      end
      for j = 1:size(maxIndexes(i))(2)
         if j>1
            //adding repetitive data to list
            maxData(counterMax,:) = ['', '', data(maxIndexes(i)(j),3), data(maxIndexes(i)(j),4), data(maxIndexes(i)(j),2), data(maxIndexes(i)(j),1)];
            counterMax = counterMax + 1;
         else
            //write data to the list (in order: physical quantity, value, measurement date, measurement time, station name, data source)
            maxData(counterMax,:) = [maxValues(i,1), maxValues(i,2), data(maxIndexes(i)(j),3), data(maxIndexes(i)(j),4), data(maxIndexes(i)(j),2), data(maxIndexes(i)(j),1)];
            counterMax = counterMax + 1;
         end
      end
   end

   clc(); //screen cleaning
   mprintf('\n'); //adding a space
   actualTableWidth = sum(cellsWidthForActual) + length(cellsWidthForActual) + 1; //calculation of the width of the actual value table
   //displaying the actual value table
   PrintLine('~', actualTableWidth);
   PrintTitle('Wartości aktualne', actualTableWidth, '|');
   PrintLine('~', actualTableWidth);
   PrintData(firstHeaderlineForActual, cellsWidthForActual, '|');
   PrintData(secondHeaderlineForActual, cellsWidthForActual, '|');
   PrintData(thirdHeaderlineForActual, cellsWidthForActual, '|');
   PrintLine('~', actualTableWidth);
   for i = 1:3
      PrintData(basicInfoImgw(i), cellsWidthForActual, '|');
      PrintData(basicInfoWttr(i), cellsWidthForActual, '|');
   end
   PrintLine('~', actualTableWidth);

   mprintf('\n\n'); //adding a space

   extremalTableWidth = sum(cellsWidthForExtremal) + length(cellsWidthForExtremal) + 1; //calculation of the width of the minimum value table
   //displaying the minimum value table
   PrintLine('~', extremalTableWidth);
   PrintTitle('Wartości minimalne', extremalTableWidth, '|');
   PrintLine('~', extremalTableWidth);
   PrintData(headerlineForExtremal, cellsWidthForExtremal, '|');
   PrintLine('~', extremalTableWidth);
   for i = 1:size(minData)(1)
      PrintData(minData(i,:), cellsWidthForExtremal, '|');
   end
   PrintLine('~', extremalTableWidth);

   extremalTableWidth = sum(cellsWidthForExtremal) + length(cellsWidthForExtremal) + 1; //calculation of the width of the maximum value table
   //displaying the maximum value table
   PrintTitle('Wartości maksymalne', extremalTableWidth, '|');
   PrintLine('~', extremalTableWidth);
   PrintData(headerlineForExtremal, cellsWidthForExtremal, '|');
   PrintLine('~', extremalTableWidth);
   for i = 1:size(maxData)(1)
      PrintData(maxData(i,:), cellsWidthForExtremal, '|');
   end
   PrintLine('~', extremalTableWidth);

   sleep(3600000); //wait 1h

end

clear; //clearing memory
